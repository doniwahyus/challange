--DML CRUD
CREATE DATABASE challange3;

CREATE TABLE user_game (
  Id int PRIMARY KEY,
  Username varchar(50),
  Password varchar(50),
  email varchar(20)
);

CREATE TABLE User_game_biodata (
  Id int PRIMARY KEY,
  user_id int,
  player_name varchar(50),
  current_level_id int,
  player_rank varchar(20)
);

CREATE TABLE User_game_history (
  Id int PRIMARY KEY,
  user_id int,
  result varchar(20),
  time varchar(20),
  point_rank varchar(10)
);

SELECT * FROM user_game;

INSERT INTO user_game (
    id, username, password, email
) VALUES 
(12, 'laki89', 'kiki9090','ki0@mail.com'),
(13, 'maman12', 'mamayi9', 'masa00@mail.com'),
(14, 'uraaaa', 'uhuy78', 'semangat7@mail.com'),
(15, 'kimochi', 'enak99', 'mantappp@mail.com');

INSERT INTO user_game_history (
    id, result, time, point_rank, user_id
) VALUES 
(1, 'menang', '09-09-2022','+15', 12),
(2, 'menang', '09-09-2022','+10', 12),
(3, 'kalah', '09-09-2022','-15', 12),
(4, 'menang', '10-09-2022','+15', 13),
(5, 'kalah', '09-09-2022','-15', 14),
(6, 'kalah', '10-09-2022','-15', 14),
(7, 'menang', '09-09-2022','+15', 15),
(8, 'menang', '09-09-2022','+15', 15);

INSERT INTO user_game_biodata (
    id, player_name, current_level_id, player_rank, user_id
) VALUES 
(1, 'Evos_keren', 'Level 12','Gold', 12),
(2, 'INA_Latif', 'Level 50','Mythical Glory', 14),
(3, 'Kukuruyuk', 'Level 100','Epic', 13),
(4, 'RRQ_Wahyu', 'Level 88','Mythic IV', 15);

UPDATE user_game_history 
SET TIME = '11-10-2022' WHERE id = 3;

DELETE FROM user_game_history WHERE id = 6;


--DDL CREATE ALTER DROP
CREATE TABLE users (
  Id int PRIMARY KEY,
  Username varchar(50),
  Password varchar(50),
  email varchar(20)
);

ALTER TABLE user_game
ADD COLUMN email varchar(20);


DROP TABLE users;